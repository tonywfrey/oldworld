#################################################################################
# Copyright (c) 2014 Anthony Frey                                               #
# Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.   #
#################################################################################
class Enemy:
    def __init__(self):
        ###############################################
        # Health and positions                        #
        ###############################################
        self.hp = 0
        self.posx = 0
        self.posy = 0
        self.is_alive = True
        self.image_path = ""
        self.max_hp = 0
        self.is_physical = True

    ###################################################
    # This is similar to the valid move check for the #
    # map, but it adds in checks against all the      #
    # different monster spawns                        #
    ###################################################
    def IsValidMovement(self, x, y, tile, spawns):
        if tile == 4:
            return False
        if x < 0:
            return False
        if y < 0:
            return False
        for i in range(0, len(spawns)):
            if spawns[i].posx == x and spawns[i].posy == y:
                return False
        else:
            return True

    ###################################################
    # AI methods for wolfs (since wolfs are physical  #
    # based mobs, they will try to move toward the    #
    # hero in the tile direction that is closest. if  #
    # this changes in the future, then we will change #
    # these methods to support it.                    #
    ###################################################
    def AI(self, hero_x, hero_y, map_data, spawns):
        if self.CanMove(hero_x, hero_y):
            self.Move(hero_x, hero_y, map_data, spawns)

    ###############################################
    # Simple move algorithm to check if the hero  #
    # is in range. This will be used to determine #
    # if the AI method even needs to be called    #
    ###############################################
    def CanMove(self, hero_x, hero_y):
        if self.is_physical:
            tile_range = 6
            if self.posx > hero_x - tile_range and self.posx < hero_x + tile_range and self.posy > hero_y - tile_range and self.posy < hero_y + tile_range:
                if abs(hero_x - self.posx) > 1 or abs(hero_y - self.posy) > 1:
                    return True
        return False

    ########################################################################
    # This runs the move event. It will move the enemy to shorten the      #
    # farthest distance between x and y. If they are the same it defaults  #
    # to the y value                                                       #
    ########################################################################
    def Move(self, hero_x, hero_y, map_data, spawns):
        x_range = abs(hero_x - self.posx)
        y_range = abs(hero_y - self.posy)
        if x_range > y_range:
            if hero_x - self.posx > 0 and self.IsValidMovement(self.posx + 1, self.posy, map_data[self.posy][self.posx+1], spawns):
                self.posx += 1
            else:
                if self.IsValidMovement(self.posx - 1, self.posy, map_data[self.posy][self.posx-1], spawns):
                    self.posx -= 1
        else:
            if hero_y - self.posy > 0 and self.IsValidMovement(self.posx, self.posy + 1, map_data[self.posy+1][self.posx], spawns):
                self.posy += 1
            else:
                if self.IsValidMovement(self.posx, self.posy - 1, map_data[self.posy-1][self.posx], spawns):
                    self.posy -= 1

######################################################################
# Currently this class is an override of the Enemy class. This might #
# be changed later to only be json info. The reason for this is to   #
# facilitate better handling of data for future monsters to be added #
# via JSON.                                                          #
######################################################################
class Wolf(Enemy):
    def __init__(self):
        Enemy.__init__(self)
        self.hp = 10
        self.name = "Wolf"
        self.image_path = "Sprites/wolf.png"
        self.max_hp = 10
        self.is_physical = True