#################################################################################
# Copyright (c) 2014 Anthony Frey                                               #
# Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.   #
#################################################################################

import pygame
import os
import json
from pprint import pprint
import Map
import Camera
import Hero
import Enemy
import math
import Cave

from Map import *
from Camera import *
from Hero import *
from Enemy import *
from Cave import *
from random import randint

##################################################################################
# This function checks to ensure that the next tile is not water or non-existent #
##################################################################################
def IsValidMovement(x, y, tile):
    if tile == 4:
        return False
    if x < 0:
        return False
    if y < 0:
        return False
    else:
        return True

#########################################################################
# This is the function that will populate a list with all the necessary #
# information for spawners.                                             #
#########################################################################
def CreateSpawns(percentage, tileId, layerData, height, width, hero):
    spawn_list = []
    temp_row = []
    for y in range(0, height):
        for x in range(0, width):
            #Ensure that the tile is right, and it isn't on the hero
            if layerData[y][x] == tileId and x != hero.posx and y != hero.posy:
                rnum = randint(1, 1000)
                if rnum <= percentage:
                    temp_wolf = Wolf()
                    temp_wolf.posx = x
                    temp_wolf.posy = y
                    spawn_list.append(temp_wolf)

    #pprint(spawn_list)
    return spawn_list

#######################################################################
# This is the main function. First everything will be initialized.    #
# After initialization, the game loop will begin running              #
#######################################################################
def main():
    #initialize pygame
    pygame.init()

    #######################################
    # Load json and serialize             #
    #######################################
    json_data = open("Maps/OldWorldTest.json").read()
    layer1_data = json.loads(json_data)
    temp_list = []
    for x in layer1_data['layers'][0]['data']:
        temp_list.append(x)

    ######################################
    # setup the map details like loading #
    # the map into a multi-dimensional   #
    # list. Since python doesn't         #
    # natively support multi-dimensional #
    # lists or arrays, you need to use a #
    # list of lists                      #
    ######################################
    map_height = layer1_data['height']
    map_width = layer1_data['width']
    tile_height = 32
    tile_width = 32
    iterator = 0
    layer1 = Layer()
    for y in range(0, layer1_data['height']):
        tempRow = []
        for x in range(0, layer1_data['width']):
            tempRow.append(temp_list[iterator])
            iterator += 1
        layer1.map.append(tempRow)

    ######################################################
    # Instantiate Ruins and caves.                       #
    ######################################################
    caves = []
    CAVE_PERCENT = 1
    CAVE_AMOUNT = 5
    while len(caves) < CAVE_AMOUNT:
        for y in range(0, map_height):
            tempCaves = []
            for x in range(0, map_width):
                if layer1.map[y][x] == 2:
                    cave_random = randint(1, 100)
                    if cave_random <= CAVE_PERCENT:
                        temp_cave = Cave(x, y, "Sprites/Cave.png", "cave")
                        caves.append(temp_cave)

    #######################################################
    # generate the cave floor plans and sizes to allow    #
    # for a more random feel                              #
    #######################################################
    for i in range(len(caves)):
        caves[i].GenerateCave()
    pprint(caves[0].map_data)

    #init the hero
    hero = Hero()
    #hero.x = cam.x + (hero.offset_x * tile_width)
    hero.posx = 32
    hero.posy = 0
    hero_image = pygame.image.load("Sprites/TempHero.png")
    is_hero_turn = True

    ###########################################################
    # Create the spawns in initial overworld. This might need #
    # moved to a seperate class later.                        #
    ###########################################################
    spawns = CreateSpawns(100, 1, layer1.map, 50, 50, hero)

    ###########################################################
    # Setup screen resolution and details.                    #
    ###########################################################
    screen = pygame.display.set_mode((1024, 768))
    pygame.display.set_caption("Old Word")

    #Bool for while loop
    running = True

    #Load OverWorld Music : This has been commented out till license details can be figured out.
    #pygame.mixer.music.load("Music/FG.wav")
    #pygame.mixer.music.play(loops=-1, start=0.0)

    ###########################################################
    # Load in the background image which is a png of the map. #
    ###########################################################
    image = pygame.image.load("Maps/OldWorldTest.png")
    #screen.blit(image, (0, 0))
    #pygame.display.flip()

    #Camera
    cam = Camera()
    cam.posx = 27
    cam.posy = 5
    cam.x -= 27 * 32
    cam.y -= -5 * 32

    turnTicker = 20


    #################################################################
    # Basic colors temp for ui until artwork is available           #
    #################################################################
    GREY = (80, 80, 80)
    BLACK = (0, 0, 0)
    LIGHT_GREY = (204, 204, 204)

    #################################################################
    # Font                                                          #
    #################################################################
    FONT = pygame.font.Font(None, 22)
    full_screen = FONT.render("Full Screen", True, (255, 255, 255,))

    #################################################################
    # Game Loop starts here. All events should be checked, and      #
    # rendering handled from here on out.                           #
    #################################################################
    while running:
        for event in pygame.event.get():
            ####################################################
            # If a quit event is fired stop the loop and allow #
            # the program to end.                              #
            ####################################################
            if event.type == pygame.QUIT:
                running = False

            #####################################################
            # Check for different key down events. These events #
            # will trigger different actions inside the game    #
            #####################################################
            if event.type == pygame.KEYDOWN and is_hero_turn:

                #######################################################################
                # If left key is down, offset the background by + 32. The map is the  #
                # object we are moving around the screen. This allows the entire map  #
                # to be rendered in one call and the prevents us from making 11 X 11  #
                # tile blits to the screen every frame. As a result, we have to move  #
                # the map in the opposite direction of what the hero would move to    #
                # to achieve the same effect. The hero's position variables will be   #
                # updated as normal to be able to track its position as if it had     #
                # actually been the on to move                                        #
                #######################################################################
                if event.key == pygame.K_LEFT and hero.posx - 1 >= 0:
                    if IsValidMovement(hero.posy, hero.posx - 1, layer1.map[hero.posy][hero.posx - 1]):
                        is_hero_turn = False
                        cam.x = cam.x + 32
                        cam.posx = cam.posx - 1
                        hero.posx = hero.posx - 1
                ############################################################
                # If right key is down, offset the background by -32 and   #
                # update the hero position accordingly.                    #
                ############################################################
                if event.key == pygame.K_RIGHT and hero.posx + 1 < 50:
                    if IsValidMovement(hero.posy, hero.posx + 1, layer1.map[hero.posy][hero.posx + 1]):
                        is_hero_turn = False
                        cam.x = cam.x - 32
                        cam.posx += 1
                        hero.posx += 1
                ############################################################
                # If the down key is pressed, offset the map by -32 and    #
                # update the hero position accordingly.                    #
                ############################################################
                if event.key == pygame.K_DOWN and hero.posy + 1 < 50:
                    if IsValidMovement(hero.posy + 1, hero.posx, layer1.map[hero.posy + 1][hero.posx]):
                        is_hero_turn = False
                        cam.y = cam.y - 32
                        cam.posy += 1
                        hero.posy += 1
                ############################################################
                # If the down key is pressed, offset the map by 32 and     #
                # update the hero position accordingly.                    #
                ############################################################
                if event.key == pygame.K_UP and hero.posy - 1 >= 0:
                    if IsValidMovement(hero.posy - 1, hero.posx, layer1.map[hero.posy - 1][hero.posx]):
                        is_hero_turn = False
                        cam.y = cam.y + 32
                        cam.posy -= 1
                        hero.posy -= 1

                #debug print() remove these later
                print("hero Y: " + str(hero.posy))
                print("Map Tile: " + str(layer1.map[hero.posy][hero.posx]))
                print("Cam X: " + str(cam.x))
                print("Cam Y: " + str(cam.y))
                print("hero X: " + str(hero.posx))
            if not is_hero_turn:
                for i in range (0, len(spawns)):
                    spawns[i].AI(hero.posx, hero.posy, layer1.map, spawns)

                is_hero_turn = True

        #############################################################################
        # Begin the process of rendering. Start by filling the screen black. Render #
        # the map then the hero. After the hero, we need to be displaying the       #
        # appropriate enemies and finally the UI.                                   #
        #############################################################################

        #render a black screen first
        screen.fill((0,0,0))

        #render map
        screen.blit(image, (cam.x, cam.y))

        #render caves/ruins
        for i in range(0, len(caves)):
            if abs(caves[i].posx - hero.posx) <= 5 and abs(caves[i].posy - hero.posy) <= 5:
                c_x = (caves[i].posx - hero.posx) * 32 + (32 * 5)
                c_y = (caves[i].posy - hero.posy) * 32 + (32 * 5)
                cave_image = pygame.image.load(caves[i].image_path)
                screen.blit(cave_image, (c_x, c_y))
                
        #render hero
        screen.blit(hero_image, (32 * 5, 32 * 5))

        #Render mobs
        for i in range(0,len(spawns)):
            if spawns[i].posx >= hero.posx - 5 and spawns[i].posx <= hero.posx + 5 and spawns[i].posy >= hero.posy - 5 and spawns[i].posy <= hero.posy + 5:
                en_x = (spawns[i].posx - hero.posx) * 32 + (32 * 5)
                en_y = (spawns[i].posy - hero.posy) * 32 + (32 * 5)
                enemy_image = pygame.image.load(spawns[i].image_path)
                screen.blit(enemy_image, (en_x, en_y))

        #Render ui covering
        pygame.draw.rect(screen, LIGHT_GREY, (352, 0, 672, 768))
        pygame.draw.rect(screen, LIGHT_GREY, (0, 352, 1024, 416))
        pygame.draw.rect(screen, GREY, (352, 0, 672, 352), 3)
        pygame.draw.rect(screen, GREY, (0, 352, 1024, 416), 3)
        #screen.blit(full_screen, (384, 32))
        pygame.display.flip()


if __name__ == "__main__":
    main()