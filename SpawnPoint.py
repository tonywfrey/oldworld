#################################################################################
# Copyright (c) 2014 Anthony Frey                                               #
# Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.   #
#################################################################################
class SpawnPoint:
    def __init__(self):
        self.posx = 0
        self.posy = 0