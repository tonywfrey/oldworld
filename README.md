# Old World Readme #

Old world is an open source project that is trying to build an old school crpg. It is licensed under the GPL and will be available to anyone who wants it.

## Requirements ##

* Python 3 (32-bit)
* Pygame

Note: that Python and Pygame will have their own requirements. Please see the licensing information in docs/COPYING for more information about the license.

Due to the pre-alpha nature, the requirements could change at anytime. 
