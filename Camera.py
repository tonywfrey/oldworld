#################################################################################
# Copyright (c) 2014 Anthony Frey                                               #
# Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.   #
#################################################################################
class Camera():
    def __init__(self):
        self.x = 0
        self.y = 0
        self.posx = 0
        self.posy = 0;