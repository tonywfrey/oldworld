#################################################################################
# Copyright (c) 2014 Anthony Frey                                               #
# Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.   #
#################################################################################
__author__ = 'anthony'

import math

from random import randint

##################################################
# Caves Class. This is will be a class to handle #
# the overworld rendering of caves and to know   #
# which cave is is which                         #
##################################################
class Cave:
    def __init__(self, x, y, image_string, cave_type):
        self.posx = x
        self.posy = y
        self.image_path = image_string
        self.type = cave_type
        self.cave_size = 0
        self.height = 0
        self.width = 0
        self.map_data = [] #this is multidimensional
        self.exit_x = 0
        self.exit_y = 0
        self.chest_x = 0
        self.chest_y = 0
        
    ##################################################
    # Function to generate cave size: 1 = small      #
    #                                 2 = medium     #
    #                                 3 = large      #
    ##################################################    
    def SetSize(self):
        self.cave_size = randint(1, 3)
        if self.cave_size == 1:
            self.height = randint(10, 15)
            self.width = randint(10, 15)
        if self.cave_size == 2:
            self.height = randint(16, 20)
            self.width = randint(16, 20)
        else:
            self.height = randint(21, 25)
            self.width = randint(21, 25)
    
    ####################################################
    # Function to generate the caves. It is my attempt #
    # at drunkard walking                              #
    ####################################################
    def GenerateCave(self):
        # Set the size
        self.SetSize()
        # set all tiles to 0
        print('setting tiles')
        for y in range(0, self.height):
            temp_row = []
            for x in range(0, self.width):
                temp_row.append(0)
            self.map_data.append(temp_row)
        #Set start point for walk
        start_x = randint(1, self.width) - 1
        start_y = randint(1, self.height) - 1
        #variables needed for walk
        changed = 0
        total = self.height * self.width
        percent_needed = 70
        dirrection = randint(1, 4) #1: north, 2: east, 3: south, 4: west
        #set the start point to 1
        self.map_data[start_y][start_x] = 1
        #the following loop will use float casting in case of python 2 port
        print('changing tiles')
        print(str(float(percent_needed)/100))
        while float(changed)/total < float(percent_needed)/100:
            previous_dir = dirrection
            if dirrection == 1 and start_y > 0:
                start_y -= 1
                changed += 1
                self.map_data[start_y][start_x] = 1
            if dirrection == 2 and start_x < self.width - 1:
                start_x += 1
                changed += 1
                self.map_data[start_y][start_x] = 1
            if dirrection == 3 and start_y < self.height - 1:
                start_y += 1
                changed += 1
                self.map_data[start_y][start_x] = 1
            if dirrection == 4 and start_x > 0:
                start_x -= 1
                changed += 1
                self.map_data[start_y][start_x] = 1
            dirrection = randint(1, 100)
            if dirrection <= 80:
                dirrection = previous_dir
            else:
                dirrection = randint(1, 4)
        #run through the map over and over until an entrance is generate
        entrance_generated = False
        while not entrance_generated:
            for y in range(0, self.height):
                if not entrance_generated:
                    for x in range(0, self.width):
                        gen = randint(1, 1000)
                        if gen == 1:
                            self.exit_x = x
                            self.exit_y = y
                            entrance_generated = True
                else:
                    break