#################################################################################
# Copyright (c) 2014 Anthony Frey                                               #
# Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.   #
#################################################################################
class Hero():
    def __init__(self):
        ##########################################
        # Positioning                            #
        ##########################################
        self.x = 0
        self.y = 0
        self.offset_x = 5
        self.offset_y = 5
        self.posx = 9
        self.posy = 9

        #########################################
        # Health and Mana                       #
        #########################################
        self.hp = 0
        self.mp = 0

        #########################################
        # Skills (magics, weapons, etc.         #
        #########################################
        self.one_hand = 0
        self.two_hand = 0
        self.polearm = 0
        self.light_armor = 0
        self.med_armor = 0
        self.heavy_armor = 0

        #########################################
        # Weapons and equipment                 #
        #########################################
        self.hand_one = 0
        self.hand_two = 0
        self.head = 0
        self.legs = 0
        self.chest = 0
        self.inventory = []